﻿namespace IME.NET.Tests.Model
{
	public class Transliterate
	{
		public string Input { get; set; }
		public string Output { get; set; }
		public string Description { get; set; }
		public bool AltGr { get; set; }
	}
}
