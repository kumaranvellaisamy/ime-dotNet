﻿using System.Collections.Generic;

namespace IME.NET.Android
{
	public static class Keyboards
	{
		private static Dictionary<string, string> _mappings = new Dictionary<string, string>();

		static Keyboards()
		{
			_mappings.Add("am-transliteration", "am-transliteration");
			_mappings.Add("ar-kbd", "ar-kbd");
			_mappings.Add("as-avro", "as-avro");
			_mappings.Add("as-bornona", "as-bornona");
			_mappings.Add("as-inscript", "as-inscript");
			_mappings.Add("as-inscript2", "as-inscript2");
			_mappings.Add("as-phonetic", "as-phonetic");
			_mappings.Add("as-transliteration", "as-transliteration");
			_mappings.Add("be-kbd", "be-kbd");
			_mappings.Add("be-latin", "be-latin");
			_mappings.Add("be-transliteration", "be-transliteration");
			_mappings.Add("ber-tfng", "ber-tfng");
			_mappings.Add("bn-avro", "bn-avro");
			_mappings.Add("bn-inscript", "bn-inscript");
			_mappings.Add("bn-inscript2", "bn-inscript2");
			_mappings.Add("bn-nkb", "bn-nkb");
			_mappings.Add("bn-probhat", "bn-probhat");
			_mappings.Add("brx-inscript", "brx-inscript");
			_mappings.Add("cyrl-palochka", "cyrl-palochka");
			_mappings.Add("da-normforms", "da-normforms");
			_mappings.Add("de", "de");
			_mappings.Add("el-kbd", "el-kbd");
			_mappings.Add("eo-h-f", "eo-h-f");
			_mappings.Add("eo-h", "eo-h");
			_mappings.Add("eo-plena", "eo-plena");
			_mappings.Add("eo-q", "eo-q");
			_mappings.Add("eo-transliteration", "eo-transliteration");
			_mappings.Add("eo-vi", "eo-vi");
			_mappings.Add("eo-x", "eo-x");
			_mappings.Add("fi-transliteration", "fi-transliteration");
			_mappings.Add("fo-normforms", "fo-normforms");
			_mappings.Add("gu-inscript", "gu-inscript");
			_mappings.Add("gu-inscript2", "gu-inscript2");
			_mappings.Add("gu-phonetic", "gu-phonetic");
			_mappings.Add("gu-transliteration", "gu-transliteration");
			_mappings.Add("he-kbd", "he-kbd");
			_mappings.Add("he-standard-2012-extonly", "he-standard-2012-extonly");
			_mappings.Add("he-standard-2012", "he-standard-2012");
			_mappings.Add("hi-bolnagri", "hi-bolnagri");
			_mappings.Add("hi-inscript", "hi-inscript");
			_mappings.Add("hi-phonetic", "hi-phonetic");
			_mappings.Add("hi-transliteration", "hi-transliteration");
			_mappings.Add("hi-soni", "hi-soni");
			_mappings.Add("hr-kbd", "hr-kbd");
			_mappings.Add("hy-kbd", "hy-kbd");
			_mappings.Add("ipa-sil", "ipa-sil");
			_mappings.Add("is-normforms", "is-normforms");
			_mappings.Add("jv-transliteration", "jv-transliteration");
			_mappings.Add("ka-kbd", "ka-kbd");
			_mappings.Add("ka-transliteration", "ka-transliteration");
			_mappings.Add("kk-arabic", "kk-arabic");
			_mappings.Add("kk-kbd", "kk-kbd");
			_mappings.Add("kn-inscript", "kn-inscript");
			_mappings.Add("kn-inscript2", "kn-inscript2");
			_mappings.Add("kn-kgp", "kn-kgp");
			_mappings.Add("kn-transliteration", "kn-transliteration");
			_mappings.Add("kok-inscript2", "kok-inscript2");
			_mappings.Add("ks-Kbd", "ks-kbd");
			_mappings.Add("ks-inscript", "ks-inscript");
			_mappings.Add("lo-kbd", "lo-kbd");
			_mappings.Add("mai-inscript", "mai-inscript");
			_mappings.Add("ml-inscript", "ml-inscript");
			_mappings.Add("ml-inscript2", "ml-inscript2");
			_mappings.Add("ml-transliteration", "ml-transliteration");
			_mappings.Add("ml-swanalekha", "ml-swanalekha");
			_mappings.Add("mn-cyrl", "mn-cyrl");
			_mappings.Add("mr-inscript", "mr-inscript");
			_mappings.Add("mr-inscript2", "mr-inscript2");
			_mappings.Add("mr-phonetic", "mr-phonetic");
			_mappings.Add("mr-transliteration", "mr-transliteration");
			_mappings.Add("my-kbd", "my-kbd");
			_mappings.Add("ne-inscript", "ne-inscript");
			_mappings.Add("ne-inscript2", "ne-inscript2");
			_mappings.Add("ne-transliteration", "ne-transliteration");
			_mappings.Add("no-normforms", "no-normforms");
			_mappings.Add("no-tildeforms", "no-tildeforms");
			_mappings.Add("or-inscript", "or-inscript");
			_mappings.Add("or-inscript2", "or-inscript2");
			_mappings.Add("or-lekhani", "or-lekhani");
			_mappings.Add("or-phonetic", "or-phonetic");
			_mappings.Add("or-transliteration", "or-transliteration");
			_mappings.Add("pa-inscript", "pa-inscript");
			_mappings.Add("pa-inscript2", "pa-inscript2");
			_mappings.Add("pa-jhelum", "pa-jhelum");
			_mappings.Add("pa-phonetic", "pa-phonetic");
			_mappings.Add("pa-transliteration", "pa-transliteration");
			_mappings.Add("reverse-ta-transliteration", "reverse-ta-transliteration");
			_mappings.Add("ru-jcuken", "ru-jcuken");
			_mappings.Add("ru-kbd", "ru-kbd");
			_mappings.Add("sa-inscript", "sa-inscript");
			_mappings.Add("sa-inscript2", "sa-inscript2");
			_mappings.Add("sa-transliteration", "sa-transliteration");
			_mappings.Add("sah-transliteration", "sah-transliteration");
			_mappings.Add("se-normforms", "se-normforms");
			_mappings.Add("si-singlish", "si-singlish");
			_mappings.Add("si-wijesekara", "si-wijesekara");
			_mappings.Add("sk-kbd", "sk-kbd");
			_mappings.Add("sr-kbd", "sr-kbd");
			_mappings.Add("sv-normforms", "sv-normforms");
			_mappings.Add("ta-99", "ta-99");
			_mappings.Add("ta-bamini", "ta-bamini");
			_mappings.Add("ta-inscript", "ta-inscript");
			_mappings.Add("ta-inscript2", "ta-inscript2");
			_mappings.Add("ta-transliteration", "ta-transliteration");
			_mappings.Add("te-inscript", "te-inscript");
			_mappings.Add("te-inscript2", "te-inscript2");
			_mappings.Add("te-kachatathapa", "te-kachatathapa");
			_mappings.Add("te-transliteration", "te-transliteration");
			_mappings.Add("th-kedmanee", "th-kedmanee");
			_mappings.Add("th-pattachote", "th-pattachote");
			_mappings.Add("transliteration", "transliteration");
			_mappings.Add("ua-kbd", "ua-kbd");
			_mappings.Add("ug-kbd", "ug-kbd");
			_mappings.Add("ur-transliteration", "ur-transliteration");
			_mappings.Add("uz-kbd", "uz-kbd");
		}

		public static string Get(string name)
		{
			return _mappings.ContainsKey(name) ? _mappings[name] : null;
		}
	}
}
