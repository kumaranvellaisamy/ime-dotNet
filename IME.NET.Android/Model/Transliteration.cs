﻿using System.Collections.Generic;

namespace IME.NET.Android.Model
{
	public class Transliteration
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Author { get; set; }
		public string Description { get; set; }
		public string Version { get; set; }
		public int MaxKeyLength { get; set; }
		public int ContextLength { get; set; }
		public IList<Pattern> Patterns { get; set; }
	}
}
