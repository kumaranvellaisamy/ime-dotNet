﻿using IME.NET.Android.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace IME.NET.Android
{
	public class Transliterator
	{
		private Transliteration _transliteration;

		public Transliterator(Transliteration transliteration)
		{
			this._transliteration = transliteration;
		}

		public static Transliterator FromName(string name)
		{
			if (Keyboards.Get(name) == null)
			{
				throw new ArgumentException("No such input method exists!");
			}

			var fileName = string.Format("{0}.json", name);
			var fileContent = ResourceLoader.GetEmbeddedResourceString(Assembly.GetAssembly(typeof(Transliterator)), fileName);
			var transliteration = JsonConvert.DeserializeObject<Transliteration>(fileContent);
			return new Transliterator(transliteration);
		}

		public static int FirstDivergence(string str1, string str2)
		{
			int length = str1.Length > str2.Length ? str2.Length : str1.Length;
			for (int i = 0; i < length; i++)
			{
				if (str1[i] != str2[i])
				{
					return i;
				}
			}

			return length - 1; // Default
		}

		public string GetAuthor()
		{
			return _transliteration != null ? _transliteration.Author : string.Empty;
		}

		public int GetContextLength()
		{
			return _transliteration != null ? _transliteration.ContextLength : 0;
		}

		public string GetDescription()
		{
			return _transliteration != null ? _transliteration.Description : string.Empty;
		}

		public string GetId()
		{
			return _transliteration != null ? _transliteration.Id : string.Empty;
		}

		public int GetMaxKeyLength()
		{
			return _transliteration != null ? _transliteration.MaxKeyLength : 0;
		}

		public string GetName()
		{
			return _transliteration != null ? _transliteration.Name : string.Empty;
		}

		public string GetVersion()
		{
			return _transliteration != null ? _transliteration.Version : string.Empty;
		}

		public string Transliterate(string input, string context, bool altGr)
		{
			if (_transliteration != null && _transliteration.Patterns != null)
			{
				foreach (var pattern in _transliteration.Patterns)
				{
					Match inputMatcher = pattern.InputPattern.Match(input);

					if (inputMatcher.Success)
					{
						if (pattern.ContextPattern == null || pattern.ContextPattern.Match(context).Success)
						{
							if (pattern.AltGr == altGr)
							{
								return ReplaceAll(inputMatcher, input, pattern.Replacement);
							}
						}
					}
				}
			}

			return input;
		}

		public String TransliterateAll(string input, IList<bool> altGr)
		{
			string curOutput = "";
			string replacement;
			string context = "";
			bool curAltGr = false;

			for (int i = 0; i < input.Length; i++)
			{
				string c = input[i].ToString();
				int startPos = curOutput.Length > this.GetMaxKeyLength() ? curOutput.Length - this.GetMaxKeyLength() : 0;
				string toReplace = curOutput.Substring(startPos) + c;
				curAltGr = altGr != null && altGr.Count > i && altGr[i];
				replacement = this.Transliterate(toReplace, context, curAltGr);
				int divIndex = Transliterator.FirstDivergence(toReplace, replacement);
				replacement = replacement.Substring(divIndex);
				curOutput = curOutput.Substring(0, startPos + divIndex) + replacement;

				context += c;
				if (context.Length > this.GetContextLength())
				{
					context = context.Substring(context.Length - this.GetContextLength());
				}
			}

			return curOutput;
		}

		private string ReplaceAll(Match matcher, string input, string replacement)
		{
			// Process substitution string to replace group references with groups
			int cursor = 0;
			StringBuilder result = new StringBuilder();

			while (cursor < replacement.Length)
			{
				char nextChar = replacement[cursor];
				if (nextChar == '\\')
				{
					cursor++;
					nextChar = replacement[cursor];
					result.Append(nextChar);
					cursor++;
				}
				else if (nextChar == '$')
				{
					// Skip past $
					cursor++;

					// The first number is always a group
					int refNum = (int)replacement[cursor] - '0';

					if ((refNum < 0) || (refNum > 9))
						throw new ArgumentException("Illegal group reference");

					cursor++;

					// Capture the largest legal group string
					bool done = false;
					while (!done)
					{
						if (cursor >= replacement.Length)
						{
							break;
						}

						int nextDigit = replacement[cursor] - '0';
						if ((nextDigit < 0) || (nextDigit > 9))
						{
							// not a number
							break;
						}

						int newRefNum = (refNum * 10) + nextDigit;
						if (matcher.Groups.Count < newRefNum)
						{
							done = true;
						}
						else
						{
							refNum = newRefNum;
							cursor++;
						}
					}

					// Append group
					if (matcher.Groups != null)
						result.Append(matcher.Groups[refNum].Value);
				}
				else
				{
					result.Append(nextChar);
					cursor++;
				}
			}

			return input.Replace(matcher.Value, result.ToString());
		}
	}
}