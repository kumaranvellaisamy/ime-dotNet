﻿using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace IME.NET.Model
{
	public class Pattern
	{
		private Regex _inputPattern = null;
		private Regex _contextPattern = null;

		[JsonIgnore]
		public Regex InputPattern
		{
			get
			{
				if (_inputPattern == null)
					_inputPattern = new Regex(Input + "$");

				return _inputPattern;
			}
		}

		[JsonIgnore]
		public Regex ContextPattern
		{
			get
			{
				if (_contextPattern == null && !string.IsNullOrEmpty(Context))
					_contextPattern = new Regex(Context + "$");

				return _contextPattern;
			}
		}

		public string Input { get; set; }

		public string Context { get; set; }

		public string Replacement { get; set; }

		public bool AltGr { get; set; }
	}
}
