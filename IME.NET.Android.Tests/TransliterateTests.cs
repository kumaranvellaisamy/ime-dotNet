﻿using IME.NET.Android.Tests.Model;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

namespace IME.NET.Android.Tests
{
	[TestFixture]
	public class TransliterateTests
	{
		[Test]
		public void AmTransliterationTest()
		{
			TransliterationTest("am-transliteration");
		}

		[Test]
		public void BeLatinTest()
		{
			TransliterationTest("be-latin");
		}

		[Test]
		public void BeTransliterationTest()
		{
			TransliterationTest("be-transliteration");
		}

		[Test]
		public void BnAvroTest()
		{
			TransliterationTest("bn-avro");
		}

		[Test]
		public void BnProbhatTest()
		{
			TransliterationTest("bn-probhat");
		}

		[Test]
		public void CyrlPalochkaTest()
		{
			TransliterationTest("cyrl-palochka");
		}

		[Test]
		public void DeTest()
		{
			TransliterationTest("de");
		}

		[Test]
		public void GuTransliterationTest()
		{
			TransliterationTest("gu-transliteration");
		}

		[Test]
		public void HeStandard2012ExtonlyTest()
		{
			TransliterationTest("he-standard-2012-extonly");
		}

		[Test]
		public void HiInscriptTest()
		{
			TransliterationTest("hi-inscript");
		}

		[Test]
		public void HiTransliterationTest()
		{
			TransliterationTest("hi-transliteration");
		}

		[Test]
		public void IpaSilTest()
		{
			TransliterationTest("ipa-sil");
		}

		[Test]
		public void JvTransliterationTest()
		{
			TransliterationTest("jv-transliteration");
		}

		[Test]
		public void KaTransliterationTest()
		{
			TransliterationTest("ka-transliteration");
		}

		[Test]
		public void KnKgpTest()
		{
			TransliterationTest("kn-kgp");
		}

		[Test]
		public void KnTransliterationTest()
		{
			TransliterationTest("kn-transliteration");
		}

		[Test]
		public void MlInscriptTest()
		{
			TransliterationTest("ml-inscript");
		}

		[Test]
		public void MlTransliterationTest()
		{
			TransliterationTest("ml-transliteration");
		}

		[Test]
		public void MrInscript2Test()
		{
			TransliterationTest("mr-inscript2");
		}

		[Test]
		public void MrTransliterationTest()
		{
			TransliterationTest("mr-transliteration");
		}

		[Test]
		public void OrInscriptTest()
		{
			TransliterationTest("or-inscript");
		}

		[Test]
		public void OrLekhaniTest()
		{
			TransliterationTest("or-lekhani");
		}

		[Test]
		public void OrTransliterationTest()
		{
			TransliterationTest("or-transliteration");
		}

		[Test]
		public void PaTransliterationTest()
		{
			TransliterationTest("pa-transliteration");
		}

		[Test]
		public void SaTransliterationTest()
		{
			TransliterationTest("sa-transliteration");
		}

		[Test]
		public void SiWijesekaraTest()
		{
			TransliterationTest("si-wijesekara");
		}

		[Test]
		public void Ta99Test()
		{
			TransliterationTest("ta-99");
		}

		[Test]
		public void TaInscriptTest()
		{
			TransliterationTest("ta-inscript");
		}

		[Test]
		public void TaTransliterationTest()
		{
			TransliterationTest("ta-transliteration");
		}

		[Test]
		public void TeInscriptTest()
		{
			TransliterationTest("te-inscript");
		}

		[Test]
		public void TeTransliterationTest()
		{
			TransliterationTest("te-transliteration");
		}

		private void TransliterationTest(string name)
		{
			var fixture = GetFixture(name);
			if (fixture != null)
			{
				var transliterator = Transliterator.FromName(fixture.Transliteration);
				foreach (var transliterate in fixture.Transliterates)
				{
					var output = transliterator.TransliterateAll(transliterate.Input, new List<bool>() { transliterate.AltGr });
					Assert.AreEqual(transliterate.Output, output);
				}
			}
			else
			{
				Assert.Fail("Fixture file is not found.");
			}
		}

		private Fixture GetFixture(string name)
		{
			var file = string.Format("{0}\\Fixtures\\{1}.json", TestContext.CurrentContext.TestDirectory, name);
			if (File.Exists(file))
			{
				var fileContent = File.ReadAllText(file);
				return JsonConvert.DeserializeObject<Fixture>(fileContent);
			}

			return null;
		}
	}
}
