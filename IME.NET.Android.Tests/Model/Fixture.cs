﻿using System.Collections.Generic;

namespace IME.NET.Android.Tests.Model
{
	public class Fixture
	{
		public string Description { get; set; }
		public string Transliteration { get; set; }
		public bool Multiline { get; set; }
		public IList<Transliterate> Transliterates { get; set; }
	}
}
